module.exports = function(grunt) {
  grunt.initConfig({
    pkg: grunt.file.readJSON( 'package.json' ),

    run: {
      saver: {
        cmd: 'casperjs',
        args: [
          'src/proboard_saver.js',
          '--board-nr=YOUR-BOARD-NR',
          '--board-name=YOUR-BOARD-NAME',
          '--user=YOUR-USERNAME',
          '--password=YOUR-PASSWORD'
        ]
      }
    }
  });

  grunt.loadNpmTasks('grunt-run');

  grunt.registerTask('default', ['run:saver']);
  grunt.registerTask('save', ['run:saver']);
};
