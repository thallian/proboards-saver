# A quick and dirty ProbBoards scraper
This tool tries to scrap all the accessible data from a [Proboards Forum](https://proboards.com/).

I wrote this because I wanted to export at least the text data for an old board hosted there which some friends and I were using years ago. 
As it turned out you simply can't do that. So here we are.

The program probably does some horrible things and I can't say if it will work for every theme. But hey, it only has to work one time to get at the data.

It is able to detect attachments and images and tries to download them too.

A working [casperjs](http://casperjs.org/) installation is needed for the stuff to work.

